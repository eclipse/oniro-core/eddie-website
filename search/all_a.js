var searchData=
[
  ['security_32',['SECURITY',['../md__builds_eclipse_oniro_core_eddie_SECURITY.html',1,'']]],
  ['send_5fmessage_33',['send_message',['../classCoapClient.html#a6db00b55c5437b1b239f59c5d1e7ae3a',1,'CoapClient']]],
  ['send_5fmessage_5fand_5fwait_5fresponse_34',['send_message_and_wait_response',['../classCoapClient.html#aaa0f931079f33fda874caf691fd2c3f3',1,'CoapClient']]],
  ['start_5fserver_35',['start_server',['../classEddieEndpoint.html#a46183ccdfd5ac6799be0cf67c0c2ffc0',1,'EddieEndpoint::start_server()'],['../classCoapServer.html#a5c7f9c829a2809bbcdd737618814c9fd',1,'CoapServer::start_server()']]],
  ['stop_5fserver_36',['stop_server',['../classEddieEndpoint.html#a98c8ab9733c42b91da5d57ca3b90f046',1,'EddieEndpoint::stop_server()'],['../classCoapServer.html#a5b8430f0776f3d81d559bf056164cbc2',1,'CoapServer::stop_server()']]]
];
