var searchData=
[
  ['get_5fattributes_63',['get_attributes',['../classEddieResource.html#ab36287546947cd2c17568edc03b5b271',1,'EddieResource']]],
  ['get_5fclient_64',['get_client',['../classEddieEndpoint.html#aaaa263a103754ccbc37201f76fa1500f',1,'EddieEndpoint::get_client()'],['../classCoapServer.html#a5eb9e35199e0e9d72765ca4466cb6f13',1,'CoapServer::get_client()']]],
  ['get_5fcontext_65',['get_context',['../classCoapServer.html#af832d2ff6a571e8dd043f9de79f71d47',1,'CoapServer']]],
  ['get_5fmy_5fip_66',['get_my_ip',['../classCoapServer.html#a868d080db89c74cce694ec370b38c01e',1,'CoapServer']]],
  ['get_5fmy_5fport_67',['get_my_port',['../classCoapServer.html#a860496fc1f6bcfe099f486af92876094',1,'CoapServer']]],
  ['get_5fpath_68',['get_path',['../classEddieResource.html#a7d22791c7affe5f78618ef7b7a3210e1',1,'EddieResource']]],
  ['get_5fresources_5ffrom_5frd_69',['get_resources_from_rd',['../classEddieEndpoint.html#adb40a77a9c022410d343b655b687783e',1,'EddieEndpoint']]],
  ['get_5fresources_5fin_5flinkformat_70',['get_resources_in_linkformat',['../classCoapServer.html#ada9e1dc9ce93d16e4392ebe8ac3649f1',1,'CoapServer']]],
  ['get_5fserver_71',['get_server',['../classEddieEndpoint.html#aeaeb77ed5c8f985027b986002fa849d4',1,'EddieEndpoint']]]
];
